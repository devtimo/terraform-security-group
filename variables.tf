variable "name" {
  description = "Nome do security group"
}

variable "vpc_id" {
  description = "VPC ID"
}

variable "ingress_cidr_blocks" {
  default = ["0.0.0.0/0"]
}

variable "ingress_rules" {
  default = ["http-80-tcp", "ssh-tcp"]
}

variable "egress_rules" {
  default = ["all-all"]
}

variable "description" {
  default = "Security group para services"
}





